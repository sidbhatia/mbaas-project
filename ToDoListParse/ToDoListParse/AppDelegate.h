//
//  AppDelegate.h
//  ToDoListParse
//
//  Created by Sid Bhatia on 1/19/15.
//  Copyright (c) 2015 HP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


