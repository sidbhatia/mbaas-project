//
//  ToDoItem.h
//  ToDoParse
//
//  Created by Sid Bhatia on 12/19/14.
//
//

#import <Foundation/Foundation.h>

@interface ToDoItem : NSObject

@property NSString *itemName;
@property bool completed;
@property NSDate *dueDate;
@property NSString *objectId;

@end
