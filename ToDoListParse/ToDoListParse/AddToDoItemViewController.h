//
//  AddToDoItemViewController.h
//  ToDoParse
//
//  Created by Sid Bhatia on 12/19/14.
//
//

#import <UIKit/UIKit.h>
#import "ToDoItem.h"
#import <Parse/Parse.h>

@interface AddToDoItemViewController : UIViewController

@property ToDoItem *toDoItem;

@end
