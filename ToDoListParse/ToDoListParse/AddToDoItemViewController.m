//
//  AddToDoItemViewController.m
//  ToDoParse
//
//  Created by Sid Bhatia on 12/19/14.
//
//

#import "AddToDoItemViewController.h"
#import "ToDoItem.h"

@interface AddToDoItemViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end

@implementation AddToDoItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

 - (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
     if (sender != self.doneButton) return;
     if (self.textField.text.length > 0) {
         self.toDoItem = [[ToDoItem alloc] init];
         self.toDoItem.itemName = self.textField.text;
         self.toDoItem.completed = false;
         self.toDoItem.dueDate = self.datePicker.date;
     }
     
     //self.toDoItem.completed = false;
     
     PFObject *todo = [PFObject objectWithClassName:@"ToDoItem"];
     todo[@"itemName"] = self.toDoItem.itemName;
     [todo setObject:[NSNumber numberWithBool:self.toDoItem.completed] forKey:@"completed"];
     todo[@"dueDate"] = self.toDoItem.dueDate;
     //[todo saveInBackground];
     [todo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
         if (succeeded) {
             // The object has been saved.
             NSLog(@"To Do Item saved");
         } else {
             // There was a problem, check error.description
             NSLog(@"%@",error.description);
         }
     }];
     [todo pinInBackground];
 }

@end
