//
//  AppDelegate.h
//  ToDoListKidozen
//
//  Created by Sid Bhatia on 3/13/15.
//  Copyright (c) 2015 com.hp.es.usps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KZApplication.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) KZApplication *app;


@end

