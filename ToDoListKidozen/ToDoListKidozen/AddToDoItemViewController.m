//
//  AddToDoItemViewController.m
//  ToDoParse
//
//  Created by Sid Bhatia on 12/19/14.
//
//

#import "AddToDoItemViewController.h"
#import "ToDoItem.h"
#import "AppDelegate.h"


@interface AddToDoItemViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end

@implementation AddToDoItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

 - (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
     if (sender != self.doneButton) return;
     if (self.textField.text.length > 0) {
         self.toDoItem = [[ToDoItem alloc] init];
         self.toDoItem.itemName = self.textField.text;
         self.toDoItem.completed = NO;
         self.toDoItem.dueDate = self.datePicker.date;
     }
     
     AppDelegate *del = [[UIApplication sharedApplication] delegate];
     id storage = [del.app StorageWithName:@"ToDoItem"];
     NSArray *objs = [NSArray arrayWithObjects:self.toDoItem.itemName,self.toDoItem.completed,self.toDoItem.dueDate,nil];
     NSArray *keys = [NSArray arrayWithObjects:@"itemName",@"completed",@"dueDate", nil];
     NSDictionary *myValues = [[NSDictionary alloc] initWithObjects:objs forKeys:keys];

     [storage create:myValues completion:^(KZResponse * k) {
         //...
     }];
 }

@end
