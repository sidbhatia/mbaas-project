//
//  ToDoItem.h
//  ToDoParse
//
//  Created by Sid Bhatia on 12/19/14.
//
//

#import <Foundation/Foundation.h>

@interface ToDoItem : NSObject

@property NSString *itemName;
@property BOOL completed;
@property NSDate *dueDate;

@end
