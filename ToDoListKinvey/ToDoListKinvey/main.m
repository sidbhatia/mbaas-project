//
//  main.m
//  ToDoListKinvey
//
//  Created by Sid Bhatia on 3/11/15.
//  Copyright (c) 2015 com.hp.es.usps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
