//
//  AddToDoItemViewController.m
//  ToDoParse
//
//  Created by Sid Bhatia on 12/19/14.
//
//

#import "AddToDoItemViewController.h"
#import "ToDoItem.h"
#import <KinveyKit/KinveyKit.h>

@interface AddToDoItemViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end

@implementation AddToDoItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

 - (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
     if (sender != self.doneButton) return;
     if (self.textField.text.length > 0) {
         self.toDoItem = [[ToDoItem alloc] init];
         self.toDoItem.itemName = self.textField.text;
         self.toDoItem.completed = NO;
         self.toDoItem.dueDate = self.datePicker.date;
     }
     
     KCSAppdataStore *_store = [KCSAppdataStore storeWithOptions:@{ KCSStoreKeyCollectionName : @"ToDoItem",
                                                   KCSStoreKeyCollectionTemplateClass : [ToDoItem class]}];
     
     [_store saveObject:self.toDoItem withCompletionBlock:^(NSArray *objectsOrNil, NSError *errorOrNil) {
         if (errorOrNil != nil) {
             //save failed, show an error alert
             UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Save failed", @"Save Failed")
                                                                 message:[errorOrNil localizedFailureReason] //not actually localized
                                                                delegate:nil
                                                       cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                                       otherButtonTitles:nil];
             [alertView show];
         } else {
             //save was successful
             NSLog(@"Successfully saved todo item (id='%@').", [objectsOrNil[0] kinveyObjectId]);
         }
     } withProgressBlock:nil];
     
 }

@end
